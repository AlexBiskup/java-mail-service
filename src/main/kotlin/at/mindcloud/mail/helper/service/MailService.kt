package at.mindcloud.mail.helper.service

import at.mindcloud.mail.helper.service.entity.Message

interface MailService {

    fun send(message: Message)

}
