package at.mindcloud.mail.helper.service

import at.mindcloud.mail.helper.service.entity.Message
import at.mindcloud.mail.helper.service.entity.Message.*
import at.mindcloud.mail.helper.service.entity.Message.Content.Type.HTML
import at.mindcloud.mail.helper.service.entity.Message.Content.Type.TEXT
import org.jsoup.Jsoup
import org.springframework.mail.javamail.JavaMailSender
import javax.activation.DataHandler
import javax.mail.Header
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import javax.mail.util.ByteArrayDataSource

class JavaMailSenderMailService(
    private val javaMailSender: JavaMailSender
) : MailService {

    override fun send(message: Message) {
        val mimeMessage = javaMailSender.createMimeMessage()
        applyMessageToMimeMessage(message, mimeMessage)
        javaMailSender.send(mimeMessage)
    }

    private fun applyMessageToMimeMessage(message: Message, mimeMessage: MimeMessage) {
        message.apply {
            mimeMessage.setFrom(from)
            mimeMessage.setSubject(subject, CHARSET)
            applyRecipientsToMessage(to, mimeMessage)
            applyBodyToMessage(body, mimeMessage)
            applyHeadersToMessage(headers, mimeMessage)
        }
    }

    private fun applyRecipientsToMessage(recipients: List<Recipient>, message: MimeMessage) {
        recipients.forEach {
            message.addRecipient(it.type, InternetAddress(it.address))
        }
    }

    private fun applyHeadersToMessage(headers: List<Header>, message: MimeMessage) {
        headers.forEach {
            message.addHeader(it.name, it.value)
        }
    }

    private fun applyBodyToMessage(body: Body, message: MimeMessage) {
        val parentMultipart = MimeMultipart(MIXED_MULTIPART)
        body.apply {
            addContentToMultipart(content, parentMultipart)
            addAttachmentsToMultipart(attachments, parentMultipart)
        }
        message.setContent(parentMultipart)
    }

    private fun addContentToMultipart(content: Content, multipart: MimeMultipart) {
        content.apply {
            when (type) {
                TEXT -> addTextContentToMultipart(value, multipart)
                HTML -> addAlternativeHtmlAndExtractedTextContentToMultipart(value, multipart)
            }
        }
    }

    private fun addTextContentToMultipart(textContent: String, multipart: MimeMultipart) {
        val textBodyPart = MimeBodyPart()
        textBodyPart.setContent(textContent, TEXT_CONTENT_TYPE)
        multipart.addBodyPart(textBodyPart)
    }

    private fun addAlternativeHtmlAndExtractedTextContentToMultipart(htmlContent: String, multipart: MimeMultipart) {
        val alternativeHtmlOrTextMultipart = createAlternativeMultipartByHtmlOrExtractedText(htmlContent)
        val contentBodyPart = MimeBodyPart()
        contentBodyPart.setContent(alternativeHtmlOrTextMultipart)
        multipart.addBodyPart(contentBodyPart)
    }

    private fun createAlternativeMultipartByHtmlOrExtractedText(htmlContent: String): MimeMultipart {
        val htmlOrTextMultipart = MimeMultipart(ALTERNATIVE_MULTIPART)
        val firstToBeAddedContent = extractTextContentOfHtmlContent(htmlContent)
        addTextContentToMultipart(firstToBeAddedContent, htmlOrTextMultipart)
        addHtmlContentToMultipart(htmlContent, htmlOrTextMultipart)
        return htmlOrTextMultipart
    }

    private fun extractTextContentOfHtmlContent(htmlContent: String): String {
        return Jsoup.parse(htmlContent).text()
    }

    private fun addHtmlContentToMultipart(htmlContent: String, multipart: MimeMultipart) {
        val htmlBodyPart = MimeBodyPart()
        htmlBodyPart.setContent(htmlContent, HTML_CONTENT_TYPE)
        multipart.addBodyPart(htmlBodyPart)
    }

    private fun addAttachmentsToMultipart(attachments: List<Attachment>, multipart: MimeMultipart) {
        attachments.forEach {
            val attachmentBodyPart = createAttachmentBodyPart(it)
            multipart.addBodyPart(attachmentBodyPart)
        }
    }

    private fun createAttachmentBodyPart(attachment: Attachment): MimeBodyPart {
        val attachmentBodyPart = MimeBodyPart()
        val dataSource = ByteArrayDataSource(attachment.data, attachment.mimeType.toString())
        attachmentBodyPart.dataHandler = DataHandler(dataSource)
        attachmentBodyPart.fileName = attachment.name
        return attachmentBodyPart
    }


    companion object {
        private const val CHARSET = "UTF-8"

        private const val MIXED_MULTIPART = "mixed"

        private const val ALTERNATIVE_MULTIPART = "alternative"

        private const val TEXT_CONTENT_TYPE = "text/plain; charset=$CHARSET"

        private const val HTML_CONTENT_TYPE = "text/html; charset=$CHARSET"
    }
}


