package at.mindcloud.mail.helper.service.entity

import org.springframework.util.MimeType
import java.io.File
import javax.mail.Header
import javax.mail.Message.RecipientType
import javax.mail.internet.InternetAddress

data class Message(
    val from: InternetAddress,
    val to: List<Recipient>,
    val subject: String,
    val body: Body,
    val headers: List<Header> = listOf()
) {
    data class Recipient(
        val address: String,
        val type: RecipientType
    )

    data class Body(
        val content: Content,
        val attachments: List<Attachment>
    )

    data class Content(
        val value: String,
        val type: Type
    ) {
        enum class Type {
            HTML, TEXT
        }
    }

    class Attachment(
        val data: ByteArray,
        val mimeType: MimeType,
        val name: String
    ) {
        constructor(file: File, mimeType: MimeType, name: String = file.name) : this(file.readBytes(), mimeType, name)
    }

}






